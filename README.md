# 软件技术基础第四次作业 #
### 介绍 ###
#### 项目简介
实现一个简易命令行计算器程序。输入数字和算法后能进行四则运算加减乘除，计算结果保留到小数点后2位。

程序要求能处理用户的输入，判断异常。

程序支持可以由用户自行选择加、减、乘、除运算。
 
### 扩展功能（加分项）
实现图形化操作、结果界面。

### 结果展示
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/180850_342f6894_9782128.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/180858_b8bc5cf0_9782128.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/180907_b8729f27_9782128.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1122/180922_1eadb669_9782128.png "5.png")